<?php

include_once "vendor/autoload.php";

use Pondit\Institute\Group;
use Pondit\Institute\Mark;
use Pondit\Institute\Student;
use Pondit\Institute\Subject;
use Pondit\Institute\Teacher;


$group = new Group();
var_dump($group);

$mark = new Mark();
var_dump($mark);

$student = new Student();
var_dump($student);

$subject = new Subject();
var_dump($subject);

$teacher = new Teacher();
var_dump($teacher);